<?php

namespace App\Form;

use App\Entity\PsOrders;
use App\Entity\PsOrderStateLang;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class IndexTypeFilter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estado', EntityType::class, array(
                'class' => PsOrderStateLang::class,
                'choice_label' => 'name',
                'label' => 'Estado del envio',
                'empty_data'  => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control'
                ),
                'required' => false,
            ))
            ->add('paisEnvio', TextType::class, array(
                'label'    => 'Pais Envio',
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control'
                ),
               'required' => false,
            ))
            ->add('submit', SubmitType::class, array('attr' => array('class' => 'btn btn-primary')))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PsOrders::class,
        ]);
    }
}
