<?php

namespace App\Repository;

use App\Entity\PsOrders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PsOrders|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsOrders|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsOrders[]    findAll()
 * @method PsOrders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsOrdersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PsOrders::class);
    }

    // /**
    //  * @return PsOrders[] Returns an array of PsOrders objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsOrders
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findFiltros($value): ?PsOrders
    {

        $qb =  $this->createQueryBuilder('p')
                    ->innerJoin()
                    ->leftJoin();

        if(isset($value['estado'])){
            $qb->andWhere('p.estado')
                ->setParameter('estado', $value['estado']);
            ;
        }

        if(isset($value['paisEnvio'])){
            $qb->andWhere('p.paisEnvio = :paisEnvio')
            ->setParameter('paisEnvio', $value['paisEnvio']);

        }

        return $qb->getQuery()->getResult();
    }

}
