<?php

namespace App\Repository;

use App\Entity\PsAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PsAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsAccess[]    findAll()
 * @method PsAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsAccessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PsAccess::class);
    }

    // /**
    //  * @return PsAccess[] Returns an array of PsAccess objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsAccess
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
