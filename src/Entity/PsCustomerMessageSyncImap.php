<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCustomerMessageSyncImap
 *
 * @ORM\Table(name="ps_customer_message_sync_imap", indexes={@ORM\Index(name="md5_header_index", columns={"md5_header"})})
 * @ORM\Entity
 */
class PsCustomerMessageSyncImap
{
    /**
     * @var binary
     *
     * @ORM\Column(name="md5_header", type="binary", nullable=false)
     */
    private $md5Header;

    public function getMd5Header()
    {
        return $this->md5Header;
    }

    public function setMd5Header($md5Header): self
    {
        $this->md5Header = $md5Header;

        return $this;
    }


}
