<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPscheckoutCart
 *
 * @ORM\Table(name="ps_pscheckout_cart")
 * @ORM\Entity
 */
class PsPscheckoutCart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_pscheckout_cart", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPscheckoutCart;

    /**
     * @var int
     *
     * @ORM\Column(name="id_cart", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idCart;

    /**
     * @var string|null
     *
     * @ORM\Column(name="paypal_intent", type="string", length=20, nullable=true, options={"default"="'CAPTURE'"})
     */
    private $paypalIntent = '\'CAPTURE\'';

    /**
     * @var string|null
     *
     * @ORM\Column(name="paypal_order", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $paypalOrder = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="paypal_status", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $paypalStatus = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="paypal_funding", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $paypalFunding = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="paypal_token", type="string", length=1024, nullable=true, options={"default"="NULL"})
     */
    private $paypalToken = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="paypal_token_expire", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $paypalTokenExpire = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="paypal_authorization_expire", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $paypalAuthorizationExpire = 'NULL';

    /**
     * @var bool
     *
     * @ORM\Column(name="isExpressCheckout", type="boolean", nullable=false)
     */
    private $isexpresscheckout = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="isHostedFields", type="boolean", nullable=false)
     */
    private $ishostedfields = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;

    public function getIdPscheckoutCart(): ?int
    {
        return $this->idPscheckoutCart;
    }

    public function getIdCart(): ?int
    {
        return $this->idCart;
    }

    public function setIdCart(int $idCart): self
    {
        $this->idCart = $idCart;

        return $this;
    }

    public function getPaypalIntent(): ?string
    {
        return $this->paypalIntent;
    }

    public function setPaypalIntent(?string $paypalIntent): self
    {
        $this->paypalIntent = $paypalIntent;

        return $this;
    }

    public function getPaypalOrder(): ?string
    {
        return $this->paypalOrder;
    }

    public function setPaypalOrder(?string $paypalOrder): self
    {
        $this->paypalOrder = $paypalOrder;

        return $this;
    }

    public function getPaypalStatus(): ?string
    {
        return $this->paypalStatus;
    }

    public function setPaypalStatus(?string $paypalStatus): self
    {
        $this->paypalStatus = $paypalStatus;

        return $this;
    }

    public function getPaypalFunding(): ?string
    {
        return $this->paypalFunding;
    }

    public function setPaypalFunding(?string $paypalFunding): self
    {
        $this->paypalFunding = $paypalFunding;

        return $this;
    }

    public function getPaypalToken(): ?string
    {
        return $this->paypalToken;
    }

    public function setPaypalToken(?string $paypalToken): self
    {
        $this->paypalToken = $paypalToken;

        return $this;
    }

    public function getPaypalTokenExpire(): ?\DateTimeInterface
    {
        return $this->paypalTokenExpire;
    }

    public function setPaypalTokenExpire(?\DateTimeInterface $paypalTokenExpire): self
    {
        $this->paypalTokenExpire = $paypalTokenExpire;

        return $this;
    }

    public function getPaypalAuthorizationExpire(): ?\DateTimeInterface
    {
        return $this->paypalAuthorizationExpire;
    }

    public function setPaypalAuthorizationExpire(?\DateTimeInterface $paypalAuthorizationExpire): self
    {
        $this->paypalAuthorizationExpire = $paypalAuthorizationExpire;

        return $this;
    }

    public function getIsexpresscheckout(): ?bool
    {
        return $this->isexpresscheckout;
    }

    public function setIsexpresscheckout(bool $isexpresscheckout): self
    {
        $this->isexpresscheckout = $isexpresscheckout;

        return $this;
    }

    public function getIshostedfields(): ?bool
    {
        return $this->ishostedfields;
    }

    public function setIshostedfields(bool $ishostedfields): self
    {
        $this->ishostedfields = $ishostedfields;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->dateUpd;
    }

    public function setDateUpd(\DateTimeInterface $dateUpd): self
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }


}
