<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPscheckoutOrderMatrice
 *
 * @ORM\Table(name="ps_pscheckout_order_matrice")
 * @ORM\Entity
 */
class PsPscheckoutOrderMatrice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_order_matrice", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderMatrice;

    /**
     * @var int
     *
     * @ORM\Column(name="id_order_prestashop", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idOrderPrestashop;

    /**
     * @var string
     *
     * @ORM\Column(name="id_order_paypal", type="string", length=20, nullable=false)
     */
    private $idOrderPaypal;

    public function getIdOrderMatrice(): ?int
    {
        return $this->idOrderMatrice;
    }

    public function getIdOrderPrestashop(): ?int
    {
        return $this->idOrderPrestashop;
    }

    public function setIdOrderPrestashop(int $idOrderPrestashop): self
    {
        $this->idOrderPrestashop = $idOrderPrestashop;

        return $this;
    }

    public function getIdOrderPaypal(): ?string
    {
        return $this->idOrderPaypal;
    }

    public function setIdOrderPaypal(string $idOrderPaypal): self
    {
        $this->idOrderPaypal = $idOrderPaypal;

        return $this;
    }


}
