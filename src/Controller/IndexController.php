<?php
namespace App\Controller;

use App\Entity\PsAddress;
use App\Entity\PsCountry;
use App\Entity\PsCustomer;
use App\Entity\PsOrderDetail;
use App\Entity\PsOrders;
use App\Entity\PsOrderStateLang;
use App\Form\IndexTypeFilter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends  AbstractController
{
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $order = new PsOrders();
        $indexForm = $this->createForm(IndexTypeFilter::class, $order);
        $indexForm->handleRequest($request);
        $filtros = array();
        /*if ($indexForm->isSubmitted()) {
            $filtrosForm = $request->request->get('index_type_filter');

            if(isset($filtrosForm['estado']) && $filtrosForm['estado'] != ''){
                $filtros['estado'] = $filtrosForm['estado'];
            }

            if(isset($filtrosForm['paisEnvio']) && $filtrosForm['paisEnvio'] != ''){
                $filtros['paisEnvio'] = $filtrosForm['paisEnvio'];
            }
        }

        if(count($filtros) == 0){
            // Cambiar el findAll por una query que solamente busque por los estados pago aceptado  o enviado
            $entities = $em->getRepository(PsOrders::class)->findAll();
        } else {
            $entities = $em->getRepository(PsOrders::class)->findFiltros($filtros);
        }*/

        $entities = $em->getRepository(PsOrders::class)->findAll();

        $totalOrders = array();
        foreach ($entities as $entity){
            $orderInformation = array();
            $orderInformation['idOrder'] = $entity->getIdOrder();
            $orderInformation['fechaPedido'] = $entity->getDateAdd();

            $customer = $em->getRepository(PsCustomer::class)->find($entity->getIdCustomer());
            $orderInformation['nombreEnvio'] = $customer->getFirstName();
            $orderInformation['apellidoEnvio'] = $customer->getLastName();

            $delivery = $em->getRepository(PsAddress::class)->find($entity->getIdAddressDelivery());
            $orderInformation['direccionEnvio'] = $delivery->getAddress1().' '.$delivery->getAddress2() ;

            $country = $em->getRepository(PsCountry::class)->find($delivery->getIdCountry());
            $orderInformation['paisEnvio'] = $country->getIsoCode();

            $orderDetail = $em->getRepository(PsOrderDetail::class)->findOneBy(['idOrderDetail' => $entity ->getIdOrder()]);
            $orderInformation['productos'] = $orderDetail->getProductName();

            $orderState = $em->getRepository(PsOrderStateLang::class)->findOneBy(['idOrderState' => $entity->getCurrentState()]);
            $orderInformation['estado'] = $orderState->getName();
            $totalOrders[] = $orderInformation;
        }
        return $this->render('pedidos/dashboard.html.twig', [
            'totalOrders' => $totalOrders,
            'indexForm' => $indexForm->createView()
        ]);
    }

    public function editEstado(Request $request): Response
    {
        //Cargar pagina con selector de estados y boton de guardar
    }

    public function save(Request $request): Response
    {
        //recoger datos y guardar en bd, redireccionar a index
    }
}